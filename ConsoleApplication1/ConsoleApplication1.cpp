#include <iostream>
#include <thread>
#include <functional>
#include <mutex>
#include <condition_variable>

const uint64_t M1 = 1000;

class Application {

	std::mutex mtx_;
	std::condition_variable cv_;
	bool data_loaded_;

public:

	Application() : data_loaded_{ false } {}

	void load_data() {
		// make this thread sleep for 1 second
		std::this_thread::sleep_for(std::chrono::milliseconds(M1));
		std::cout << " --> loading data from somewhere... " << std::endl;
		// lock the loading process
		std::lock_guard<std::mutex> guard(mtx_);
		// set the flag to true, means data is loaded
		data_loaded_ = true;
		// notify the condition variable
		cv_.notify_one();
	}

	bool is_data_loaded() {
		return data_loaded_;
	}

	void main_task() {
		std::cout << " --> do some handshaking..." << std::endl;
		// acquire the lock
		std::unique_lock<std::mutex> mlock(mtx_);
		// start waiting for the condition variable to get signaled
		// wait() will internally release the lock and make the thread to block.
		// As soon as condition variable get signaled, resume the thread and
		// again acquire the lock. Then check if condition is met or not
		// If condition is met then continue else again go in wait.
		cv_.wait(mlock, std::bind(&Application::is_data_loaded, this));
		std::cout << " --> do processing on loaded data..." << std::endl;
	}
};

int main() {
	Application app;

	std::thread thread_1(&Application::main_task, &app);
	std::thread thread_2(&Application::load_data, &app);

	thread_2.join();
	thread_1.join();

	return 0;
}